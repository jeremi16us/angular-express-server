const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const User = require('../models/user');

function verifyToken(req, res, next){
    if(!req.headers.authorization){
        return res.status(401).send('Unauthorized request, NO TOKEN FOUND!');
    }

    let token = req.headers.authorization.token.spli(' ')[1];
    if(token == 'null'){
        return res.status(401).send('Unauthorized request, NO TOKEN FOUND!');
    }

    let payload = jwt.verify(token, 'secretKey');
    if(!payload){
        return res.status(401).send('Unauthorized request, TOKEN INVALID!');
    }
    req.userId = payload.subject;
    next();
}

router.get('/', function(req, res){
    res.send('Greatings from API route');
});

router.post('/login', function(req, res){
    let userData = req.body;
    
    User.findOne({email: userData.email}, function(error, matchedUser){
        if(error){
            console.error('Error!' + error);
        } else {
            if(!matchedUser){
                res.status(401).send('User not found!');
            } else if(matchedUser.password !== userData.password){
                res.status(401).send('Incorrect password');
            } else {
                let payload = { subject: matchedUser._id };
                let token = jwt.sign(payload, 'secretKey');
                res.status(200).send({"matchedUser": matchedUser, "token": token});
            }
        }
    });
});

module.exports = router;