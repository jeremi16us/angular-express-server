const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const UserRole = require('../models/userRole');
const sendResponse = require('../database/response');


router.post('/add', function(req, res){
    let userRoleData = req.body;
    let userRole = new UserRole(userRoleData);
    console.log(JSON.stringify(userRole));
    userRole.save( (error, registeredUserRole) => {
        sendResponse(error, registeredUserRole, res);
    });
});

router.post('/update', function(req, res){
    let userRoleData = req.body;
    let userRole = new UserRole(userRoleData);
    console.log(JSON.stringify(userRole));
    UserRole.updateOne({ _id: userRole._id }, userRole, (error, registeredUserRole) => {
        sendResponse(error, registeredUserRole, res);
    });
});

router.get('/get', function(req, res){
    let id = req.query.id;
    UserRole.findOne({_id:id}, (error, listOfUserRole) => {
        sendResponse(error, listOfUserRole, res);
    });
});

router.get('/list', function(req, res){
    let query = req.query.q;
    if(!query){ query =''; }

    UserRole.find({ name: new RegExp(query, 'i') }, (error, listOfUserRole) => {
        sendResponse(error, listOfUserRole, res);
    });
});

module.exports = router;