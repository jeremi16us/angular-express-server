const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const sendResponse = require('../database/response');

router.post('/add', function(req, res){
    let userData = req.body;
    let user = new User(userData);
    console.log(JSON.stringify(user));
    user.save( (error, registeredUser) => {
        let payload = { subject: registeredUser._id };
        let token = jwt.sign(payload, 'secretKey');
        let returnData = {"registeredUser": registeredUser, "token": token};
        
        sendResponse(error, returnData, res);
    });
});

router.post('/update', function(req, res){
    let userData = req.body;
    let user = new User(userData);
    console.log(JSON.stringify(user));
    User.updateOne( { _id: user._id }, user, {upsert:true}, (error, registeredUser) => {
        sendResponse(error, registeredUser, res);
    });
});

router.get('/get', function(req, res){
    let id = req.query.id;
    User.findOne({_id:id}, (error, listOfUser) => {
        sendResponse(error, listOfUser, res);
    });
});

router.get('/list', function(req, res){
    let query = req.query.q;
    if(!query){ query =''; }
    
    User.find({ first_name: new RegExp(query, 'i') }, (error, listOfUser) => {
        sendResponse(error, listOfUser, res);
    });
});

module.exports = router;