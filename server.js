const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const connection = require('./database/connection').connection;
connection().then(value=>{
    console.log('Connectced to MONGODB');
}).catch(reason=>{
    console.log(reason,"Fails to connect to MONGODB");
});

const PORT = 1228;

const api = require('./routes/api');
const apiUser = require('./routes/apiUser');
const apiUserRole = require('./routes/apiUserRole');
const app = express();
app.use(cors());

app.use(bodyParser.json());

app.use('/api', api);
app.use('/api/user', apiUser);
app.use('/api/userRole', apiUserRole);

app.get('/', function(req, res){
    res.send('Greatings from server');
});

app.listen(PORT, function(){
    console.log('Server is now running on localhost:' + PORT);
});