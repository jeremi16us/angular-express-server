const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: String,
    password: String,
    first_name: String,
    middle_name: String,
    last_name: String,
    /*created_by: String,
    created_time: Date,
    modified_by: String,
    modified_time: Date*/
});

module.exports = mongoose.model('user', userSchema, 'user');