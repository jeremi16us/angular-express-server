const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userRoleSchema = new Schema({
    name: String,
    description: String,
    /*created_by: String,
    created_time: Date,
    modified_by: String,
    modified_time: Date*/
});

module.exports = mongoose.model('userRole', userRoleSchema, 'user_role');