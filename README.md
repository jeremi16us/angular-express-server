# Angular Express Initiation Process
1. Create folder angular-express-server
2. Initiate package.json file
2.1 $ npm init --yes
3. Add dependency package express and body-parser
3.1 $ npm install express body-parser --save
4. Create new file server.js
5. Create subfolder routes and file api.js in it
6. Add dependency package mongoose
6.1 $ npm install mongoose --save
7. Add dependency package cors
7.1 $ npm install cors --save
8. Add dependency package jsonwebtoken
8.1 $ npm install jsonwebtoken --save


# USING LOCAL MONGO DB
1. open mongo shell
1.1 $ mongo
2. Show database
2.2 $ show dbs
3. Create and insert data
3.1 $ use angular_express
```
**db.user.insert()**
{
    "email":"angular@gmail.com",
    "first_name":"Angular", 
    "middle_name":"Express", 
    "last_name":"Server", 
    "password":"PasswordYaAngular"
}
```

# USING DOCKER MONGO DB
1. docker pull mongo:4.2
1.1 $ sudo docker run -d -p 27017-27019:27017-27019 -v /home/app/tools/mongo/db:/data/db -d --restart always --name truebits-mongo mongo:4.2
1.2 $ sudo docker run -d -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=PA55w0rd -p 27017-27019:27017-27019 -v /home/app/tools/mongo/db:/data/db -d --restart always --name truebits-mongo mongo:4.2
1.3 $ sudo docker-compose -f docker/mongo/mongo-compose.yml up -d

1.2.1 docker exec -it truebits-mongo bash
1.2.2 docker stop truebits-mongo
1.2.3 docker rm truebits-mongo
1.2.4 docker restart truebits-mongo