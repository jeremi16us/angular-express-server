const mongoose = require('mongoose');

var connection = async function () {
    const mongoDB = "angular_express"
    const mongoUser = "root"
    const mongoPassword = "PA55w0rd"
    const mongoURI = "mongodb://apps.truebitstech.com:27017/" + mongoDB;
    return mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });

}

module.exports.connection = connection;