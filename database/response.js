
/**
 * 
 */
const sendResponse = function(error, returnData, res){
    if(error){
        console.error('Error!' + error);
        var returnError = { message:'Error!', reason:error }
        res.status(404).json(returnError);
    } else {
        res.status(200).json(returnData);
    }
}

module.exports = sendResponse;;